-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 20 déc. 2022 à 15:56
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cryptopot`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE `administrateur` (
  `Id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`Id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Structure de la table `association`
--

CREATE TABLE `association` (
  `id` int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `creation_date` date NOT NULL,
  `mail` varchar(30) NOT NULL,
  `phone_number` varchar(30) NOT NULL,
  `adress` varchar(30) NOT NULL,
  `activity` varchar(30) NOT NULL,
  `id_user` int
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `association`
--

INSERT INTO `association` (`id`, `name`, `creation_date`, `mail`, `phone_number`, `adress`, `activity`) VALUES
(3, '30 Millions d\'Amis', '1995-01-01', '30millionsdamis@gmail.com', '0145689752', '78 rue du Général Leclerc', 'Protection des Animaux'),
(4, 'Pour l\'Enfance', '2012-02-25', 'pourlenfance@gmail.com', '0163259978', '23 place Victor Hugo ', 'Protection de l\'Enfance'),
(5, 'JDB Prevention Cancer', '2009-10-05', 'jdb.preventioncancer@gmail.com', '0144752330', '2-4 rue du Mont Louvet', 'Prevention contre le cancer'),
(6, ' 2eme Chance', '2006-02-02', '2emechance@gmail.com', '0154798625', '31-32 Quai de Dion-Bouton', 'Difficultes scolaires');

-- --------------------------------------------------------

--
-- Structure de la table `cagnotte`
--

CREATE TABLE `pot` (
  `id` int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_asso` int(6) NOT NULL,
  `name` varchar(30) NOT NULL,
  `target_amount` double NOT NULL,
  `description` varchar(50) NOT NULL,
  `amount_paid` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `cagnotte`
--

INSERT INTO `pot` (`id_asso`, `name`, `target_amount`, `description`, `amount_paid`) VALUES
(3, 'Sauver les bébés tortues', 1000000, 'Beaucoup de bébés tortues meurent chaque année car', 510656.24),
(6, 'Former des babouchiers', 500000, 'Le marché des babouches étant en plein essor, l\'as', 155555.99);

-- --------------------------------------------------------

--
-- Structure de la table `donation`
--

CREATE TABLE `donation` (
  `id` int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_pot` int(6) NOT NULL,
  `deposit_date` date NOT NULL,
  `crypto_type` varchar(20) NOT NULL,
  `amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `donation`
--

INSERT INTO `donation` (`id`, `id_pot`, `deposit_date`, `crypto_type`, `amount`) VALUES
(1, 0, '2022-02-16', 'Tether', 12000),
(2, 1, '2022-03-25', 'Thether Gold', 47656.24),
(3, 1, '2022-03-01', 'USD Coin', 55555.99),
(4, 0, '2022-05-25', 'Dai', 451000),
(5, 1, '2022-08-12', 'PAX Gold', 100000);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(6) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `roles` JSON
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `name`, `first_name`, `mail`, `password`, `roles`) VALUES
(1, 'Beniddir', 'Alicia', 'b.ali@gmail.com', 'SecretStory', '["ROLE_USER"]'),
(2, 'El Banna', 'Maha', 'maha.el@gmail.com', 'LesAnges', '["ROLE_USER"]'),
(3, 'Chalons', 'Guillaume', 'guigui@gmail.com', 'LoveIsBlind', '["ROLE_USER"]'),
(4, 'Goulahsen', 'Elisa', 'elisa.goulahsen@gmail.com', 'LesMarseillais', '["ROLE_USER"]'),
(5, 'Mbanya Nya', 'Roch', 'roch.mbanya@gmail.com', 'LesChtis', '["ROLE_USER"]'),
(6, 'Mavoulana', 'Moubina', 'mavou.moubi@gmail.com', 'LoveIsland', '["ROLE_USER"]');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `cagnotte`
--
ALTER TABLE `pot`
  ADD UNIQUE KEY `IdAsso` (`id_asso`);

--
-- Index pour la table `donation`
--
ALTER TABLE `donation`
  ADD KEY `IdPot` (`id_pot`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `administrateur`
--
ALTER TABLE `administrateur`
  ADD CONSTRAINT `administrateur_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `user` (`Id`);

--
-- Contraintes pour la table `association`
--
ALTER TABLE `association`
  ADD CONSTRAINT `association_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`Id`);

--
-- Contraintes pour la table `cagnotte`
--
ALTER TABLE `pot`
  ADD CONSTRAINT `pot_ibfk_1` FOREIGN KEY (`id_asso`) REFERENCES `association` (`id`);

--
-- Contraintes pour la table `donation`
--
ALTER TABLE `donation`
  ADD CONSTRAINT `donation_ibfk_1` FOREIGN KEY (`id_pot`) REFERENCES `pot` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
